import React, { useState, useEffect } from "react";
import Editor from "@monaco-editor/react";

interface Line {
  text: string;
  height: number;
  checked: boolean;
}

const TextEditor: React.FC = () => {
  const [text, setText] = useState<string>("");
  const [lines, setLines] = useState<Line[]>([]);

  useEffect(() => {
    const linesArray = text.split("\n").map((line) => ({
      text: line,
      height: 20,
      checked: false,
    }));
    setLines(linesArray);
  }, [text]);

  const handleTextChange = (value: string | undefined) => {
    if (value !== undefined) {
      setText(value);
    }
  };

  const handleHeightChange = (index: number, newHeight: number) => {
    const newLines = [...lines];
    newLines[index].height = newHeight;
    setLines(newLines);
  };

  const handleCheckboxChange = (index: number) => {
    const newLines = [...lines];
    newLines[index].checked = !newLines[index].checked;
    setLines(newLines);
  };

  return (
    <div className="container">
      <textarea
        className="textarea"
        value={text}
        onChange={(e) => handleTextChange(e.target.value)}
        rows={10}
      />
      <div className="controls">
        {lines.map((line, index) => (
          <div key={index} className="line-controls">
            <input
              className="number-input"
              type="number"
              value={line.height}
              onChange={(e) =>
                handleHeightChange(index, parseInt(e.target.value, 10))
              }
            />
            <input
              className="checkbox"
              type="checkbox"
              checked={line.checked}
              onChange={() => handleCheckboxChange(index)}
            />
          </div>
        ))}
      </div>
      <div className="editor-container">
        <Editor
          defaultLanguage="text"
          value={text}
          onChange={handleTextChange}
          options={{
            scrollbar: {
              vertical: "hidden",
            },
          }}
        />
      </div>
      <div className="lines-preview">
        {lines.map((line, index) => (
          <div
            key={index}
            className="line-preview"
            style={{
              height: `${line.height}px`,
              backgroundColor: line.checked ? "yellow" : "transparent",
            }}
          >
            {line.text}
          </div>
        ))}
      </div>
    </div>
  );
};

export default TextEditor;
